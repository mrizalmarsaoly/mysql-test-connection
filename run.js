const { exec } = require("child_process");
require("colors")
const dotenv = require('dotenv');
dotenv.config();

// const sleep = (ms) => {
//     return new Promise((resolve) => {
//         setTimeout(resolve, ms);
//     });
// }
let running = false
const init = async () => {
    if (running) return;
    running = true
        exec("node index.js", (err, stdout, stderr) => {
            if (err) {
                process.stdout.write("Error Service: ", String(err.bgRed).white)
                console.log("err:",err)
            }
            if (stderr) {
                process.stdout.write("Error Std Service: ", String(stderr.bgRed).white)
                console.log("stderr", stderr)
            }
            process.stdout.write(stdout.green)
            running = false
        })
     
}

setInterval(init, 60 * 1000)