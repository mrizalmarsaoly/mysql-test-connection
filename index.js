const mysql = require('mysql');
require('dotenv').config()

const config = {
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    port: process.env.MYSQL_PORT
}
const connection = mysql.createConnection(config);
exports.connect = () => connection.connect((err) => {
    if (err) {
        console.log("MySql Error: ", err.code , " from:",  process.env.MYSQL_HOST + ":" + process.env.MYSQL_PORT )
    } else {
        console.log("MySql Successfully Connected from", process.env.MYSQL_HOST + ":" + process.env.MYSQL_PORT)
        process.exit()
    }
});
this.connect()
